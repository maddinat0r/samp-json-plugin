#pragma once

#include "sdk.hpp"

#define AMX_DECLARE_NATIVE(native) \
	cell AMX_NATIVE_CALL native(AMX *amx, cell *params)

#define AMX_DEFINE_NATIVE(native) \
	{#native, Native::native},


namespace Native 
{
	AMX_DECLARE_NATIVE(test);
	AMX_DECLARE_NATIVE(json_parse);
	AMX_DECLARE_NATIVE(json_get_int);
	AMX_DECLARE_NATIVE(json_get_object);

	AMX_DECLARE_NATIVE(json_generate);
	AMX_DECLARE_NATIVE(json_set_int);
	AMX_DECLARE_NATIVE(json_set_object);
	
};
