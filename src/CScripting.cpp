#include "CScripting.hpp"
#include "CJson.hpp"
#include "sdk.hpp"

#include <rapidjson/reader.h>
#include <rapidjson/writer.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

using rapidjson::Document;
using rapidjson::StringBuffer;
using rapidjson::Writer;
using rapidjson::PrettyWriter;

#include <string>
using std::string;


// native test(dest[], max_size = sizeof dest);
AMX_DECLARE_NATIVE(Native::test)
{
	/*Value admin(kObjectType);
	admin.AddMember("level", 777, DefAlloc);
	admin.AddMember("role", "Haxx0r", DefAlloc);
	
	Value faction(kObjectType);
	faction.AddMember("id", 123, DefAlloc);
	faction.AddMember("rank", "Boss", DefAlloc);

	Value general(kObjectType);
	general.AddMember("membership", "New User", DefAlloc);
	general.AddMember("personal_tag", "Master of Disaster", DefAlloc);

	Value main(kObjectType);
	main.AddMember("admin", admin, DefAlloc);
	main.AddMember("faction", faction, DefAlloc);
	main.AddMember("general", general, DefAlloc);

	StringBuffer buffer;
	PrettyWriter<StringBuffer> writer(buffer);
	main.Accept(writer);	// Accept() traverses the DOM and generates Handler e
	
	amx_SetCString(amx, params[1], buffer.GetString(), params[2]);*/
	
	return 0;
}

// native JSON:json_parse(const src[]);
AMX_DECLARE_NATIVE(Native::json_parse)
{
	char *input = nullptr;
	amx_StrParam(amx, params[1], input);

	Document document;
	if (document.Parse<0>(input).HasParseError())
		return 0;

	ValueP_t value_ptr;
	Id_t id = CJsonManager::Get()->CreateValue(value_ptr);

	Value &value = *value_ptr;
	value = document;
	return id;
}

// native json_get_int(JSON:json, const name[]);
AMX_DECLARE_NATIVE(Native::json_get_int)
{
	/*DocumentP_t jdoc_ptr;
	if (CJsonManager::Get()->GetDocument(params[1], jdoc_ptr) == false)
		return 0;

	char *field_name = nullptr;
	amx_StrParam(amx, params[2], field_name);
	
	Document &jdoc = *jdoc_ptr;
	Value &jval = jdoc[field_name];
	if (jval.IsInt() == false)
		return 0;

	return jval.GetInt();*/
	return 0;
}
/*
// native Float:json_get_float(JSON:json, const name[]);
AMX_DECLARE_NATIVE(Native::json_generate)
{

	return 1;
}

// native json_get_string(JSON:json, const name[], dest[], max_size = sizeof dest);
AMX_DECLARE_NATIVE(Native::json_get_string)
{

	return 1;
}

// native bool:json_get_bool(JSON:json, const name[]);
AMX_DECLARE_NATIVE(Native::json_get_bool)
{

	return 1;
}
*/
// native JSON:json_get_object(JSON:json, const name[]);
AMX_DECLARE_NATIVE(Native::json_get_object)
{
	ValueP_t value_ptr;
	if (CJsonManager::Get()->GetValue(params[1], value_ptr) == false)
		return 0;

	char *field_name = nullptr;
	amx_StrParam(amx, params[2], field_name);

	Value &value = *value_ptr;
	Value &field_value = value[field_name];
	if (field_value.IsObject() == false)
		return 0;

	ValueP_t new_value_ptr;
	Id_t id = CJsonManager::Get()->CreateValue(new_value_ptr);
	new_value_ptr->SetObject() = field_value; //field_value is now invalid
	return id;
}
/*
// native JsonDatatype:json_get_type(JSON:json, const name[]);
AMX_DECLARE_NATIVE(Native::json_get_type)
{

	return 1;
}
*/


// native json_generate(JSON:json, dest[], sizeof dest);
AMX_DECLARE_NATIVE(Native::json_generate)
{
	ValueP_t value_ptr;
	if (CJsonManager::Get()->GetValue(params[1], value_ptr) == false)
		return 0;


	StringBuffer buffer;
	PrettyWriter<StringBuffer> writer(buffer);
	value_ptr->Accept(writer);

	amx_SetCString(amx, params[2], buffer.GetString(), params[3]); 
	return 1;
}

// native json_set_int(JSON:json, const name[], value);
AMX_DECLARE_NATIVE(Native::json_set_int)
{
	/*DocumentP_t jdoc_ptr;
	if (CJsonManager::Get()->GetDocument(params[1], jdoc_ptr) == false)
		return 0;

	char *field_name = nullptr;
	amx_StrParam(amx, params[2], field_name);

	int field_value = params[3];

	Document &jdoc = *jdoc_ptr;
	if (jdoc.HasMember(field_name))
	{
		Value &jval = jdoc[field_name];
		if (jval.IsInt() == false)
			return 0;

		jval = field_value;
	}
	else
		jdoc.AddMember(field_name, field_value, jdoc.GetAllocator());

	return 1;*/
	return 0;
}
/*
// native json_set_float(JSON:json, const name[], Float:value);
AMX_DECLARE_NATIVE(Native::json_set_float)
{

	return 1;
}

// native json_set_string(JSON:json, const name[], const string[]);
AMX_DECLARE_NATIVE(Native::json_set_string)
{

	return 1;
}

// native json_set_bool(JSON:json, const name[], bool:value);
AMX_DECLARE_NATIVE(Native::json_set_bool)
{

	return 1;
}
*/

//native json_set_object(JSON:json, const name[], JSON:object);
AMX_DECLARE_NATIVE(Native::json_set_object)
{
	ValueP_t value_ptr;
	if (CJsonManager::Get()->GetValue(params[1], value_ptr) == false)
		return 0;

	ValueP_t field_value_ptr;
	if (CJsonManager::Get()->GetValue(params[3], field_value_ptr) == false)
		return 0;

	char *field_name = nullptr;
	amx_StrParam(amx, params[2], field_name);


	Value &value = *value_ptr;
	Value &field_value = *field_value_ptr;
	if (value.HasMember(field_name))
	{
		Value &value_target = value[field_name];
		if (value_target.IsObject() == false)
			return 0;

		value_target = field_value;
		CJsonManager::Get()->DeleteValue(params[3]);
	}
	else
	{
		auto &alloc = CJsonManager::Get()->GetAllocator();
		value.AddMember(field_name, field_value, alloc);
		//TODO: check if we need to delete field_value here too
	}
	return 1;
}
