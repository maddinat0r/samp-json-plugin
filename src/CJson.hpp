#pragma once

#include "CSingleton.hpp"

#include <map>
#include <memory>

using std::map;

#include <rapidjson/rapidjson.h>
#include <rapidjson/document.h>

using rapidjson::Value;


using Id_t = unsigned int;
using ValueP_t = std::shared_ptr<Value>;


class CJsonManager : public CSingleton<CJsonManager>
{
public:
	CJsonManager() :
		m_Alloc()
	{ }
	~CJsonManager() = default;

private:
	map<Id_t, ValueP_t> m_Values;
	rapidjson::MemoryPoolAllocator<> m_Alloc;

public:
	Id_t CreateValue(ValueP_t &dest);
	bool GetValue(Id_t id, ValueP_t &dest);
	bool DeleteValue(Id_t id);

	decltype(m_Alloc) &GetAllocator()
	{
		return m_Alloc;
	}
};
