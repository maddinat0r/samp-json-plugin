#include "CJson.hpp"

Id_t CJsonManager::CreateValue(ValueP_t &dest)
{
	Id_t id = 1;
	while (m_Values.find(id) != m_Values.end())
		++id;

	dest = m_Values.emplace(id, std::make_shared<Value>()).first->second;
	return id;
}

bool CJsonManager::GetValue(Id_t id, ValueP_t &dest)
{
	auto it = m_Values.find(id);
	if (it != m_Values.end())
	{
		dest = it->second;
		return true;
	}
	return false;
}

bool CJsonManager::DeleteValue(Id_t id)
{
	return m_Values.erase(id) == 1;
}