#include "sdk.hpp"
#include "CScripting.hpp"
#include "version.hpp"


extern void	*pAMXFunctions;
logprintf_t logprintf;


PLUGIN_EXPORT unsigned int PLUGIN_CALL Supports() 
{
	return SUPPORTS_VERSION | SUPPORTS_AMX_NATIVES/* | SUPPORTS_PROCESS_TICK*/; 
}

PLUGIN_EXPORT bool PLUGIN_CALL Load(void **ppData) 
{
	pAMXFunctions = ppData[PLUGIN_DATA_AMX_EXPORTS];
	logprintf = (logprintf_t)ppData[PLUGIN_DATA_LOGPRINTF];
	
	logprintf(" >> plugin.json: v" JSON_VERSION " successfully loaded.");
	return true;
}

PLUGIN_EXPORT void PLUGIN_CALL Unload() 
{
	logprintf("plugin.json: Unloading plugin...");

	logprintf("plugin.json: Plugin unloaded."); 
}
/*
PLUGIN_EXPORT void PLUGIN_CALL ProcessTick() 
{
	
}*/


extern "C" const AMX_NATIVE_INFO native_list[] = 
{
	AMX_DEFINE_NATIVE(test)
	AMX_DEFINE_NATIVE(json_parse)
	AMX_DEFINE_NATIVE(json_get_int)
	AMX_DEFINE_NATIVE(json_get_object)

	AMX_DEFINE_NATIVE(json_generate)
	AMX_DEFINE_NATIVE(json_set_int)
	AMX_DEFINE_NATIVE(json_set_object)
	
	{NULL, NULL}
};

PLUGIN_EXPORT int PLUGIN_CALL AmxLoad(AMX *amx) 
{
	return amx_Register(amx, native_list, -1);
}

PLUGIN_EXPORT int PLUGIN_CALL AmxUnload(AMX *amx) 
{
	return AMX_ERR_NONE;
}
